const gameContainer = document.getElementById("game");

const COLORS = [
  "red",
  "blue",
  "green",
  "orange",
  "purple",
  "red",
  "blue",
  "green",
  "orange",
  "purple",
];

// here is a helper function to shuffle an array
// it returns the same array with values shuffled
// it is based on an algorithm called Fisher Yates if you want ot research more
function shuffle(array) {
  let counter = array.length;

  // While there are elements in the array
  while (counter > 0) {
    // Pick a random index
    let index = Math.floor(Math.random() * counter);

    // Decrease counter by 1
    counter--;

    // And swap the last element with it
    let temp = array[counter];
    array[counter] = array[index];
    array[index] = temp;
  }

  return array;
}

let shuffledColors = shuffle(COLORS);

// this function loops over the array of colors
// it creates a new div and gives it a class with the value of the color
// it also adds an event listener for a click for each card
function createDivsForColors(colorArray) {
  for (let color of colorArray) {
    // create a new div
    const newDiv = document.createElement("div");

    // give it a class attribute for the value we are looping over
    newDiv.classList.add(color);
    newDiv.classList.add("hide");

    // call a function handleCardClick when a div is clicked on
    newDiv.addEventListener("click", handleCardClick);

    // append the div to the element with an id of game
    gameContainer.append(newDiv);
  }
}

// TODO: Implement this function!
function handleCardClick(event) {
  // you can use event.target to see which element was clicked
  let numberOfCardsRevealed = getRevealedCount();
  /**
   * two cases: reveal a blank card or toggle a colored card
   * a blank card can be revealed only if ONE other card is revealed.
   * a colored card can be toggled without any restrictions.
   * 
   * 
   * cards will stay revealed for 2s if not matched.
   */
  
  if (numberOfCardsRevealed < 2 || !event.target.classList.contains("hide")) {
    event.target.classList.toggle("hide");
    removeClickListener(event.target);        /* to prevent toggle using click before timeout */
    setTimeout(hideCard, 2000, event.target); 
  }
  checkForAMatch(); 
}

function getRevealedCount() {
  let count = 0;
  document.querySelectorAll("div div").forEach((div) => {
    if(!div.classList.contains("highlight")) { /* if card is not matched yet */
      if (!div.classList.contains("hide")) {   /* if card is revealed already */ 
        count++;
      }
    }
  });
  console.log("getRevealedCount ", count); 
  return count;
}

function checkForAMatch() {
  let numberOfCardsRevealed = getRevealedCount();
  if (numberOfCardsRevealed === 2) {
    
    let cardDetails = getRevealedCards();

    if(checkRevealedCards(cardDetails)) {
      console.log("it's a match.");
      highlightRevealedCards(cardDetails);
    }
  }
}

function getRevealedCards() {
  let colorsRevealed = [];

  /* find out the revealed colors */
  document.querySelectorAll("div div").forEach((div) => {
    if(!div.classList.contains("highlight")) { /* card is not matched yet */
      if (!div.classList.contains("hide")) { /* card is revealed */ 
        colorsRevealed.push([div, div.classList[0]]);
      }
    }
  });
  console.log("getRevealedCards ", colorsRevealed); 
  return colorsRevealed;
    // remove eventlistener
    // add freeze class name to skip beint counted by function
  
}

function checkRevealedCards(cardArray) {
  console.log("checkRevealedCards ", cardArray);
  if (cardArray[0][1] === cardArray[1][1]) {
    return true;
  }
  else {
    return false; 
  }
}

function highlightRevealedCards(cardArray) {
  console.log("highlightRevealedCards ", cardArray);
  cardArray.forEach(div => div[0].classList.add("highlight"));
  checkGameOver(); 
}

function removeClickListener(card) {
  card.removeEventListener("click", handleCardClick);
}

function hideCard(card) {
  if(!card.classList.contains("highlight")) { /* if card has not been matched */
    card.classList.toggle("hide");
    card.addEventListener("click", handleCardClick); /* re-attach click listener after card is toggled back */
  } 
}

function checkGameOver() {
  let gameOver = 1; 
  document.querySelectorAll("div div").forEach((div) => {
    if(!div.classList.contains("highlight")) { /* the card is not matched and game is not over yet */
      gameOver = 0;
    }
  });
  if(gameOver) {
    document.querySelector("h1").innerText = "Game Over! Hit Refresh!";
  }
}

// when the DOM loads
createDivsForColors(shuffledColors);
